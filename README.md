## Merge Scripts

**Website:**

https://www.nexusmods.com/fallout4/mods/53420

**Repository:**

https://gitlab.com/hobby-projects3623613/source-merge-scripts

### Requirements

**Powershell 7.3+**

https://github.com/PowerShell/PowerShell/releases/download/v7.3.2/PowerShell-7.3.2-win-x64.msi]

**BSArch** 

https://www.nexusmods.com/newvegas/mods/64745?tab=files

**7zip v21+**

https://www.7-zip.org/
