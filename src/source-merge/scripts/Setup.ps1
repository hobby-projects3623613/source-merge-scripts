# Package: 		Merge Scripts
# Author:  		Brazenvoid
# Description: 	Creates sources archive and sets up work folders

. $PSScriptRoot/BaseScript.ps1
. $PSScriptRoot/BaseConfiguration.ps1
. $PSScriptRoot/BaseSourceMerge.ps1

$Global:ScriptName = 'Setup'
$Global:TotalTasks = 3

IntroduceScript
try {
	# -----------------
	IntimateTaskStart 'Setting up work folders...'

	CreateOrEmptyFolder $ModsOptimizePath
	CreateFolder 		$ModsPath
	CreateOrEmptyFolder $ModsWorkspacePath
	CreateFolder		$SourcesPath
	CreateFolder		$TemplatesPath
	CreateFolder		$TrashPath
	CreateOrEmptyFolder	$WorkspacePath
	
	# -----------------
	IntimateTaskStart 'Gathering information...'

	$RepairMode = (Get-ChildItem $TemplatesPath).Count -gt 0
	if ($RepairMode) {
		LineBreak
		Write-Host 'Existing template detected, running in repair mode...' -ForegroundColor Magenta
		$DataFolderPath = GetDataFolderPath
		$MaximumBA2Size = (GetMaximumBA2Size) / 1073741824
	} else {
		$CurrentTemplate = ''
		$DataFolderPath  = ''
		$MaximumBA2Size  = 4.0
	}

	$DataFolderPath =
	    InquirePath `
	    -Subject 'Please drag and drop the fallout 4 data folder in this window and then press enter.' `
	    -Help "This path will be used for sourcing and replacing archives." `
	    -DefaultValue $DataFolderPath

    $MaximumBA2Size =
        Inquire `
        -Subject 'Maximum Size of BA2s' `
        -Help "If you don't know about it then 4GB is a safe bet." `
        -Question 'Please enter limit in GBs' `
        -DefaultValue $MaximumBA2Size

    if ($RepairMode) {

        $ExistingTemplates = Get-ChildItem $TemplatesPath -Directory
        if ($ExistingTemplates.Length -eq 1) {
            $CurrentTemplateChoice = 1
        } else {
            $CurrentTemplateChoice =
                MultiChoice -Subject 'Choose current deployed template:' -Options $ExistingTemplates -Cancellable
        }
        if ($CurrentTemplateChoice -ne $False) {
            $CurrentTemplate = $ExistingTemplates[$CurrentTemplateChoice - 1].BaseName
        }
    }

	if (!$RepairMode) {

		$RunMerge =
		    Confirm `
		    -Subject 'Do you want to merge mods into source afterwards?' `
		    -Help 'Configures and continues to Source Merge script after completing setup.' `
		    -DefaultYes

		if ($RunMerge) {

			$TemplateName =
			    Inquire `
			    -Subject 'Set a descriptive name for the template.' `
			    -Help 'Used to identify the template.' `
			    -Question 'Name' `
			    -DefaultValue 'Merge'

			InquireLoadOrderManagement -Path $ModsPath
		}

		IntimateLongWaitTime
	}
	
	# -----------------
	IntimateTaskStart 'Setting up performance profile...'

	$DriveLetter      = $pwd.Path[0]
	$IsSSD            = $False
	$ParallelBSArch   = 2
	$ParallelRobocopy = 8

	if (ValidateSSDVolume $pwd.Path) {
	    $IsSSD     = $True
	    $CoreCount = GetCoreCount

        if ($CoreCount -le 16) {
            $ParallelBSArch = $CoreCount - 1
        } else {
            $ParallelBSArch = 32
        }
        $ParallelRobocopy = 128
	}
	
	$Configuration = @{
		fallout4 = @{
			dataFolderPath = $DataFolderPath
			maximumBA2Size = [Math]::Floor([Convert]::ToSingle($MaximumBA2Size) * 1.4 * 1073741824)
		}
		performance = @{
			isSSD            = $IsSSD
			parallelBSArch   = $ParallelBSArch
			parallelRobocopy = $ParallelRobocopy
		}
		templates = @{
			activeTemplate = $CurrentTemplate
		}
	}
	OverwriteConfiguration $Configuration

	if ($RepairMode) {
		IntimateAllTasksCompletion 'Repair completed successfully!'
	} else {

	    # -----------------
        IntimateTaskStart 'Moving source archives...'

        ForEach ($AssetArchive in $F4AssetArchives) {

            $SourceArchivePath = $SourcesPath + '\' + $AssetArchive.Name + '.ba2'
            $ArchivePath       = $DataFolderPath + '\' + $AssetArchive.Name + '.ba2'

            if (Test-Path $ArchivePath) {
                Write-Host ('Moving ' + $AssetArchive.Name + '.ba2...')
                Move-Item -Path $ArchivePath -Destination $SourcesPath
            }
        }

		if ($RunMerge) {
			IntimateAllTasksCompletion 'Setup completed successfully!' -NoExitWrapper
			& .\dist\SourceMerge.ps1 -ForceTemplateName $TemplateName
		} else {
			IntimateAllTasksCompletion 'Setup completed successfully!'
		}
	}
}
catch {	IntimateTaskError $_ } 