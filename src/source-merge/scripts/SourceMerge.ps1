# Package: 		Merge Scripts
# Author:  		Brazenvoid
# Description: 	Performs the sources merger

Param([string]$ForceTemplateName = '')

. $PSScriptRoot/BaseScript.ps1
. $PSScriptRoot/BaseConfiguration.ps1
. $PSScriptRoot/BaseModOptimization.ps1
. $PSScriptRoot/BaseMerge.ps1
. $PSScriptRoot/BaseSourceMerge.ps1

$Global:ScriptName = 'Source Merge'
	
IntroduceScript
try {	
	# -----------------
	IntimateTaskStart 'Initializing...'

	RoboRemove $WorkspacePath
	RoboRemove $ModsWorkspacePath
	RoboRemove $ModsOptimizePath

	if ((GetActiveTemplateName) -ne '') {

		$CurrentTemplate = Get-Item ("$TemplatesPath\" + (GetActiveTemplateName))
		if ($CurrentTemplate.Count) {
			$Global:CurrentTemplate = $CurrentTemplate[0]
		} else {
			ResetActiveTemplate
		}
	}

	Function Merge ([switch]$ReplaceActive, [string]$ForceTemplateName = '') {

		IntroduceScript

		if ($ReplaceActive) {

			$Global:TotalTasks = 5
			$TemplateName = ($Global:CurrentTemplate.Basename -Split '-')[1].Trim()

			InquireLoadOrderManagement -Path $ModsPath
			IntimateLongWaitTime
		}
		elseif ($ForceTemplateName -ne '') {

			$Global:TotalTasks = 6
			$TemplateName = $ForceTemplateName
		}
		else {

			$Global:TotalTasks = 7

			# -----------------
			IntimateTaskStart 'Gathering information...'

			$TemplateName =
			    Inquire `
                -Subject 'Set a descriptive name for the template.'`
                -Help 'Used to identify the template.'`
                -Question 'Name'`
                -DefaultValue 'Merge'

			InquireLoadOrderManagement -Path $ModsPath

			IntimateLongWaitTime
		}
		$Template = GenerateTemplateFolder $TemplateName

		# -----------------
		IntimateTaskStart 'Extracting source archives...'

		ExtractSources

		# -----------------
		IntimateTaskStart 'Optimize and merge mods...'

		OptimizeAndMergeMods

		# -----------------
		if (((GetActiveTemplateName) -ne '') -and !$ReplaceActive) {

			IntimateTaskStart 'Moving back current template archives...'

			UnsetTemplate $Global:CurrentTemplate

		} else {

			IntimateTaskStart 'Cleaning up template assets...'

			CleanupTemplateExtras

            if ($ReplaceActive) {
                CreateOrEmptyFolder $Global:CurrentTemplate.FullName
            }
		}
		ResetActiveTemplate

		# -----------------
		IntimateTaskStart 'Generating template sources...'

		GenerateTemplateSources $Template

		# -----------------
		if (Test-Path "$ModsWorkspacePath\*") {
			IntimateTaskStart 'Handling extra assets...'

			GenerateExtraAssetsMod -To $Global:DataFolderPath
		} else {
			IntimateTaskStart 'No extra assets found...'
		}
		
		# -----------------	
		IntimateTaskStart 'Saving state...'

		ApplyTemplate $Template
		UpdateActiveTemplate $Template
		
		# -----------------	
		IntimateAllTasksCompletion 'Merge template created successfully!' -NoExitWrapper

		Write-Host "`n`nPress enter to go back to template management menu..." -ForegroundColor Green -NoNewLine
		Read-Host
	}
	
	Function RemoveTemplates {
		ShowHeader

		$RemoveTemplate = ChooseTemplate
		if ($RemoveTemplate) {
			$Global:TotalTasks = 1
			ShowHeader

			# -----------------
			IntimateTaskStart "Removing $RemoveTemplate.Basename template..."

			Remove-Item $RemoveTemplate.FullName -Recurse
		}
	}

	Function Reset {
		$Global:TotalTasks = 3
		IntroduceScript
		IntimateLongWaitTime

		# -----------------
		if ($Global:CurrentTemplate) {

			IntimateTaskStart 'Moving back current template archives...'

			UnsetTemplate $Global:CurrentTemplate
			ResetActiveTemplate

		} else {

			IntimateTaskStart 'Cleaning up any existing extra asset archives...'

			Get-ChildItem $From -Filter ($MergedSourceModName + "*") | ForEach-Object { Remove-Item $_.FullName }
		}

		# -----------------
		IntimateTaskStart 'Handling source archives...'

		ResetSources
		
		# -----------------
		IntimateTaskStart 'Saving state...'
	}
	
	Function ShowHeader {
		IntroduceScript
		Write-Host "`nCurrent Template: " -ForegroundColor Magenta -NoNewLine
		Write-Host ($Global:CurrentTemplate ? $Global:CurrentTemplate.Basename : 'No template applied.') -ForegroundColor Cyan
	}
	
	Function SwitchTemplate {
		ShowHeader
		
		$SwitchToTemplate = ChooseTemplate
		if ($SwitchToTemplate) {
			ShowHeader
			
			if ($Global:CurrentTemplate) {
				$Global:TotalTasks = 2
				
				# -----------------
				IntimateTaskStart 'Moving back current template archives...'
				
				UnsetTemplate $Global:CurrentTemplate
			} else {
				$Global:TotalTasks = 1
			}
			
			# -----------------
			IntimateTaskStart ('Moving ' + $SwitchToTemplate.Basename + ' template archives...')

			ApplyTemplate $SwitchToTemplate
			
			UpdateActiveTemplate $SwitchToTemplate
		}
	}
	
	if (!($Global:CurrentTemplate -or (Get-ChildItem $TemplatesPath -Directory).Count)) {
		IntroduceScript
		Merge -ForceTemplateName $ForceTemplateName
	}
	
	IntroduceScript
	do {
		$Global:CurrentTask = 1
		$StartTime          = Get-Date
		ShowHeader
		
		$OpChoice =
		    MultiChoice `
            -Subject 'Choose the action to perform:'`
            -Options @('Create New Template', 'Replace Active Template', 'Switch Template', 'Remove Templates', 'Reset')`
            -OptOutLabel 'Exit'`
            -Default $(if ((GetActiveTemplateName) -ne '') {2} else {1})

		switch ($OpChoice) {
			1 { Merge }
			2 { Merge -ReplaceActive }
			3 { SwitchTemplate }
			4 { RemoveTemplates }
			5 { Reset }
		}
	} while ($OpChoice -ne 0)
}
catch {	IntimateTaskError $_ }