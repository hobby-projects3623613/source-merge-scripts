# Package: 		Merge Scripts
# Author:  		Brazenvoid
# Description: 	Houses common methods for source merges

# Configfuration

Function GetActiveTemplateName {
    Return $Global:Configuration.templates.activeTemplate
}

Function GetDataFolderPath {
    Return $Global:Configuration.fallout4.dataFolderPath
}

Function SetActiveTemplateName([string]$Name) {
    $Global:Configuration.templates.activeTemplate = $Name
}

# Logic

Function ExtractSources {

    $F4AssetArchives | ForEach-Object -ThrottleLimit (GetParallelBSArchLimit) -Parallel {

        . $using:PSScriptRoot/BaseScript.ps1

        $ArchivePath = $SourcesPath + '\' + $_.Name + '.ba2'
        if (Test-Path $ArchivePath) {

            $ArchiveFolderPath = $WorkspacePath + '\' + $_.Name
            CreateFolder $ArchiveFolderPath
            ExtractBA2 -ArchivePath $ArchivePath -Destination $ArchiveFolderPath
        }
    }
}

# Merge

Function GenerateExtraAssetsMod ([string]$To) {

    GenerateMergeModArchives $MergedSourceModName

    $ExtraAssetsHandlingState = 0

    if ((Get-ChildItem $ModsWorkspacePath -File -Filter "*.ba2").Count -gt 0) {

        Copy-Item -Path $MergedModESLPluginPath -Destination ($To + '\' + $MergedModArchives[[MergedModArchivesEnum]::Plugin].Filename) -Force
        MoveAssetArchives -From $ModsWorkspacePath -To $To
        $ExtraAssetsHandlingState += 1
    }
    if ((Get-ChildItem $ModsWorkspacePath).Count -gt 0) {

        Pack7z -ArchivePath ("$To\" + $MergedModArchives[[MergedModArchivesEnum]::PluginsArchive].Filename) -TargetPath $ModsWorkspacePath -EmptySource
        RoboRemove $ModsWorkspacePath
        $ExtraAssetsHandlingState += 2
    }

    if (($ExtraAssetsHandlingState -eq 1) -or ($ExtraAssetsHandlingState -eq 3)) {
        IntimateAction `
        -Action ('Please open your mod manager and enable '+ $MergedModArchives[[MergedModArchivesEnum]::Plugin].Filename)`
        -Help 'Your assortment of mods contain assets that are not included in source archives thus they have been packed in a separate extra assets mod.'
    }
    if (($ExtraAssetsHandlingState -eq 2) -or ($ExtraAssetsHandlingState -eq 3)) {
        IntimateFilesystemAction `
        -Action ('Press enter to open the Fallout 4 Data folder from where you must load ' + $MergedModArchives[[MergedModArchivesEnum]::PluginsArchive].Filename + ' into your mod manager')`
        -Help 'Your assortment of mods contain plugins or other incompatible files which have been packed inside that archive.`nDo not delete the archive from the data folder.'`
        -Path $To
    }
}

Function MergeModsIntoSource {

    IntimateSubTaskStart 'Merging mods into sources...'

    Get-ChildItem $WorkspacePath | Sort-Object | Select-Object -ExpandProperty FullName | ForEach-Object {
        RoboOverride $ModsWorkspacePath $_
    }
    CreateFolder $ModsWorkspacePath

}

Function OptimizeAndMergeMods {

    $ModCounter     = 1
    $TotalModsCount = (Get-ChildItem $ModsPath).Count
    $NewModsCount   = (Get-ChildItem $ModsPath -Exclude "*-$OptimizedModIdentifier.*").Count

    Write-Host ('Found: ' + $TotalModsCount.ToString() + ' mods, ' + $NewModsCount.ToString() + " mods may need optimization.`n")

    Get-ChildItem $ModsPath | Sort-Object | ForEach-Object {

        $AlreadyMerged  = $false

        Write-Host ($ModCounter.ToString() + '/' + $TotalModsCount.ToString() + ' - Validating ' +  $_.Basename + '...') -ForegroundColor Magenta

        if ($_.Basename -NotLike "*-$OptimizedModIdentifier") {
            OptimizeModArchive -ModsSourcePath $ModsPath -Archive $_ -ArchiveOptimizedMods $true
        } else {
            Extract7z -ArchivePath $_.FullName -Destination $ModsWorkspacePath
        }

        if (OptimizeForSSD) {
            $Size = GetSizeInGigabytes $ModsWorkspacePath
            if ($Size -gt 10.0) {
                MergeModsIntoSource
                $AlreadyMerged  = $true
            }
        }

        $ModCounter++
    }

    if (!$AlreadyMerged) {
        MergeModsIntoSource
    }
}

# Templates

Function ApplyTemplate ($Template) {
    MoveAssetArchives -From $Template.FullName -To $Global:DataFolderPath
}

Function ChooseTemplate {

    $Templates = Get-ChildItem $TemplatesPath -Directory -Exclude $Global:CurrentTemplate.Basename
    $ChosenTemplate =
    MultiChoice `
        -Subject 'Choose template to switch to:'`
        -Options ($Templates | Select-Object -ExpandProperty Basename)`
        -Cancellable

    if (($ChosenTemplate -ne 0) -and [Microsoft.VisualBasic.Information]::IsNumeric($ChosenTemplate)) {
        Return $Templates[([int]$ChosenTemplate) - 1]
    }
    Return $False
}

Function CleanupTemplateExtras ([switch]$ReplaceActive) {

    Foreach ($File in $MergedModArchives) {
        if (Test-Path ($Global:DataFolderPath + '\' + $File.Filename)) {
            Remove-Item ($Global:DataFolderPath + '\' + $File.Filename) -Force
        }
    }
}

Function GenerateTemplateFolder ([string]$TemplateName = 'Merge') {

    $Date = $(Get-Date)
    $Path = $TemplatesPath + '\'
    $Path += $Date.Year.ToString() + '.'
    $Path += $Date.Month.ToString().PadLeft(2, '0') + '.'
    $Path += $Date.Day.ToString().PadLeft(2, '0') + ' '
    $Path += $Date.Hour.ToString().PadLeft(2, '0') + '.'
    $Path += $Date.Minute.ToString().PadLeft(2, '0') + ' - '
    $Path += $TemplateName + ' - '
    $Path += (Get-ChildItem $ModsPath).Count.ToString() + ' Mods'

    CreateFolder $Path
    return (Get-Item $Path)[0]
}

Function GenerateTemplateSources ($Template) {

    $F4AssetArchives | ForEach-Object -ThrottleLimit (GetParallelBSArchLimit) -Parallel {

        . $using:PSScriptRoot/BaseScript.ps1
        . $using:PSScriptRoot/BaseConfiguration.ps1
        . $using:PSScriptRoot/BaseSourceMerge.ps1

        $ArchiveFilename = $_.Name + '.ba2'
        $TargetPath      = $WorkspacePath + '\' + $_.Name
        $ArchivePath     = $Global:DataFolderPath + '\' + $ArchiveFilename

        PackBA2 -TargetPath $TargetPath -ArchivePath $ArchivePath -Type $_.Type -IsSourceBA2 -RemoveSource
    }
}

Function MoveAssetArchives ([string]$From, [string]$To) {

    Foreach ($AssetArchive in $F4AssetArchives) {

        $ArchivePath         = $From + '\' + $AssetArchive.Name + '.ba2'
        $SourceArchiveExists = Test-Path $ArchivePath

        if ($SourceArchiveExists) {
            Move-Item -Path $ArchivePath -Destination $To -Force
        }
    }

    Get-ChildItem $From -Filter ($MergedSourceModName + "*") | ForEach-Object {
        Move-Item -Path $_.FullName -Destination $To -Force
    }
}

Function UnsetTemplate ($Template) {
    MoveAssetArchives -From $Global:DataFolderPath -To $Template.FullName
}

Function ResetActiveTemplate {
    SetActiveTemplateName ''
    SaveConfiguration
    $Global:CurrentTemplate = $False
}

Function ResetSources() {

    Foreach ($AssetArchive in $F4AssetArchives) {
        $ArchivePath = $SourcesPath + '\' + $AssetArchive.Name + '.ba2'
        if (Test-Path $ArchivePath) {
            Write-Host ('Copying ' + $AssetArchive.Name + '.ba2...')
            Copy-Item -Path $ArchivePath -Destination ($Global:DataFolderPath + '\' + $AssetArchive.Name + '.ba2') -Force
        }
    }
}

Function UpdateActiveTemplate ($NewTemplate) {
    SetActiveTemplateName $NewTemplate.Basename
    SaveConfiguration
    $Global:CurrentTemplate = $NewTemplate
}

#Environment

$Global:CurrentTemplate = $false
$Global:DataFolderPath  = GetDataFolderPath