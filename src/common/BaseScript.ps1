# Package: Merge Scripts
# Author:  Brazenvoid

$ErrorActionPreference = 'Stop'
$StartTime = Get-Date

$Global:CurrentTask = 1
$Global:ScriptName = ''
$Global:TotalTasks = 1

$ConfigurationPath = '.\dist\Configuration.json'
$ModsPath = '.\mods'
$ModsToMergePath = '.\mods-merge'
$ModsOptimizePath = '.\mods-optimize'
$ModsWorkspacePath = '.\mods-workspace'
$SourcesPath = '.\sources'
$TemplatesPath = '.\templates'
$TrashPath = '.\dist\trash'
$WorkspacePath = '.\workspace'

# Fallout 4 Environment

$F4AssetArchives = @(
	@{Name = 'DLCCoast - Main'; 			Type = 'general'}
	@{Name = 'DLCCoast - Textures'; 		Type = 'textures'}
	@{Name = 'DLCCoast - Voices_en'; 		Type = 'general'}
	@{Name = 'DLCNukaWorld - Main'; 		Type = 'general'}
	@{Name = 'DLCNukaWorld - Textures'; 	Type = 'textures'}
	@{Name = 'DLCNukaWorld - Voices_en'; 	Type = 'general'}
	@{Name = 'DLCRobot - Main'; 			Type = 'general'}
	@{Name = 'DLCRobot - Textures'; 		Type = 'textures'}
	@{Name = 'DLCRobot - Voices_en'; 		Type = 'general'}
	@{Name = 'DLCworkshop01 - Main'; 		Type = 'general'}
	@{Name = 'DLCworkshop01 - Textures'; 	Type = 'textures'}
	@{Name = 'DLCworkshop02 - Main'; 		Type = 'general'}
	@{Name = 'DLCworkshop02 - Textures'; 	Type = 'textures'}
	@{Name = 'DLCworkshop03 - Main'; 		Type = 'general'}
	@{Name = 'DLCworkshop03 - Textures'; 	Type = 'textures'}
	@{Name = 'DLCworkshop03 - Voices_en'; 	Type = 'general'}
	@{Name = 'Fallout4 - Animations'; 		Type = 'compressed'}
	@{Name = 'Fallout4 - Interface'; 		Type = 'general'}
	@{Name = 'Fallout4 - Materials'; 		Type = 'compressed'}
	@{Name = 'Fallout4 - Meshes'; 			Type = 'compressed'}
	@{Name = 'Fallout4 - MeshesExtra'; 		Type = 'compressed'}
	@{Name = 'Fallout4 - Misc'; 			Type = 'general'}
	@{Name = 'Fallout4 - Nvflex'; 			Type = 'compressed'}
	@{Name = 'Fallout4 - Shaders'; 			Type = 'general'}
	@{Name = 'Fallout4 - Sounds'; 			Type = 'general'}
	@{Name = 'Fallout4 - Startup'; 			Type = 'general'}
	@{Name = 'Fallout4 - Textures1'; 		Type = 'textures'}
	@{Name = 'Fallout4 - Textures2'; 		Type = 'textures'}
	@{Name = 'Fallout4 - Textures3'; 		Type = 'textures'}
	@{Name = 'Fallout4 - Textures4'; 		Type = 'textures'}
	@{Name = 'Fallout4 - Textures5'; 		Type = 'textures'}
	@{Name = 'Fallout4 - Textures6'; 		Type = 'textures'}
	@{Name = 'Fallout4 - Textures7'; 		Type = 'textures'}
	@{Name = 'Fallout4 - Textures8'; 		Type = 'textures'}
	@{Name = 'Fallout4 - Textures9'; 		Type = 'textures'}
	@{Name = 'Fallout4 - Voices'; 			Type = 'general'}
)

$F4AssetFolders = 'Interface', 'LODSettings', 'Materials', 'Meshes', 'Misc', 'Music', 'Programs', 'Scripts', 'ShadersFX', 'Sound', 'Strings', 'Textures', 'Vis'

# Plugin Keys - Extracted from xEdit source: https://github.com/TES5Edit/TES5Edit/blob/dev-4.1.4/Core/wbDefinitionsFO4.pas
$F4PluginKeys =   'AACT', 'ACBS', 'ACEC', 'ACEP', 'ACHR', 'ACID', 'ACPR', 'ACSR', 'ACTI', 'ACTV', 'ACUN', 'ADDN', 'AECH', 'AHCF', 'AHCM', 'AIDT', 'ALCA', 'ALCC', 'ALCH', 'ALCL', 'ALCO', 'ALCS', 'ALDI', 'ALDN', 'ALEA', 'ALED', 'ALEQ', 'ALFA', 'ALFC', 'ALFD', 'ALFE', 'ALFI', 'ALFL', 'ALFR', 'ALFV', 'ALID', 'ALLA', 'ALLS', 'ALMI', 'ALNA', 'ALNT', 'ALPC', 'ALRT', 'ALSP', 'ALST', 'ALUA', 'AMDL', 'AMMO', 'ANAM', 'ANIO', 'AOR2', 'AORU', 'APPR', 'ARMA', 'ARMO', 'ARTO', 'ASPC', 'ASTP', 'ATKD', 'ATKE', 'ATKR', 'ATKT', 'ATKS', 'ATKW', 'ATTN', 'ATTX', 'ATXT', 'AVFL', 'AVIF', 'AVSK', 'BAMT', 'BCLF', 'BIDS', 'BIPL', 'BMCT', 'BMMP', 'BNAM', 'BNDS', 'BOD2', 'BODT', 'BOOK', 'BPND', 'BPNI', 'BPNN', 'BPNT', 'BPTD', 'BPTN', 'BSIZ', 'BSMB', 'BSMP', 'BSMS', 'BTXT', 'CAMS', 'CDIX', 'CELL', 'CIS1', 'CIS2', 'CITC', 'CLAS', 'CLFM', 'CLMT', 'CLSZ', 'CMPO', 'CNTO', 'COBJ', 'COCT', 'COED', 'COLL', 'CONT', 'CPTH', 'CRDT', 'CRGR', 'CRIF', 'CRIS', 'CRVA', 'CS2H', 'CS2D', 'CS2E', 'CS2F', 'CS2K', 'CSCR', 'CSCV', 'CSDC', 'CSDI', 'CSDT', 'CSFL', 'CSGD', 'CSLR', 'CSMD', 'CSME', 'CSRA', 'CSTY', 'CTDA', 'CUSD', 'CVPA', 'DALC', 'DAMA', 'DAMC', 'DEBR', 'DELE', 'DEMO', 'DESC', 'DEST', 'DEVA', 'DFOB', 'DFTF', 'DFTM', 'DIAL', 'DLBR', 'DLVW', 'DMAX', 'DMDC', 'DMDL', 'DMDS', 'DMGT', 'DMIN', 'DNAM', 'DOBJ', 'DODT', 'DOFT', 'DOOR', 'DPLT', 'DSTA', 'DSTD', 'DSTF', 'DTGT', 'DTID', 'DUAL', 'EAMT', 'ECOR', 'ECZN', 'EDID', 'EFID', 'EFIT', 'EFSH', 'EITM', 'ENAM', 'ENCH', 'ENIT', 'EPF2', 'EPF3', 'EPFB', 'EPFD', 'EPFT', 'EQUP', 'ESCE', 'ETYP', 'EXPL', 'EYES', 'FACT', 'FCHT', 'FCPL', 'FFFF', 'FIMD', 'FLMV', 'FLOR', 'FLST', 'FLTR', 'FLTV', 'FMIN', 'FMRI', 'FMRN', 'FMRS', 'FNAM', 'FNMK', 'FNPR', 'FPRT', 'FSTP', 'FSTS', 'FTSF', 'FTSM', 'FTST', 'FTYP', 'FULL', 'FURN', 'FVPA', 'GDRY', 'GLOB', 'GMST', 'GNAM', 'GRAS', 'GREE', 'GWOR', 'HAZD', 'HCLF', 'HDPT', 'HEAD', 'HLTX', 'HNAM', 'HTID', 'ICO2', 'ICON', 'IDLA', 'IDLB', 'IDLC', 'IDLE', 'IDLF', 'IDLM', 'IDLT', 'IMAD', 'IMGS', 'IMSP', 'INAM', 'INCC', 'INDX', 'INFO', 'INGR', 'INNR', 'INRD', 'INTT', 'IOVR', 'IPCT', 'IPDS', 'ISIZ', 'ITID', 'ITMC', 'ITME', 'ITMS', 'ITXT', 'JAIL', 'JNAM', 'JOUT', 'KEYM', 'KNAM', 'KSIZ', 'KSSM', 'KWDA', 'KYWD', 'LAND', 'LAYR', 'LCEC', 'LCEP', 'LCID', 'LCPR', 'LCRT', 'LCSR', 'LCTN', 'LCUN', 'LENS', 'LFSD', 'LFSP', 'LGTM', 'LIGH', 'LLCT', 'LLKC', 'LNAM', 'LSCR', 'LSPR', 'LTEX', 'LTMP', 'LTPT', 'LTPC', 'LVLC', 'LVLD', 'LVLF', 'LVLG', 'LVLI', 'LVLM', 'LVLN', 'LVLO', 'LVSG', 'LVSP', 'MASE', 'MATO', 'MATT', 'MCHT', 'MDOB', 'MESG', 'MGEF', 'MHDT', 'MIC2', 'MICN', 'MICO', 'MISC', 'MLSI', 'MNAM', 'MO2C', 'MO2F', 'MO2S', 'MO2T', 'MO3C', 'MO3F', 'MO3S', 'MO3T', 'MO4C', 'MO4F', 'MO4S', 'MO4T', 'MO5C', 'MO5F', 'MO5S', 'MO5T', 'MOD2', 'MOD3', 'MOD4', 'MOD5', 'MODC', 'MODF', 'MODL', 'MODS', 'MODQ', 'MOVT', 'MPAI', 'MPAV', 'MPCD', 'MPGN', 'MPGS', 'MPPC', 'MPPF', 'MPPI', 'MPPK', 'MPPM', 'MPPN', 'MPPT', 'MPRT', 'MRSV', 'MSDK', 'MSDV', 'MSID', 'MSM0', 'MSM1', 'MSTT', 'MSWP', 'MTNM', 'MTYP', 'MUSC', 'MUST', 'MWGT', 'NAM0', 'NAM1', 'NAM2', 'NAM3', 'NAM4', 'NAM5', 'NAM6', 'NAM7', 'NAM8', 'NAM9', 'NAMA', 'NAME', 'NAVI', 'NAVM', 'NETO', 'NEXT', 'NNAM', 'NNGT', 'NNGS', 'NNUS', 'NNUT', 'NOCM', 'NONE', 'NOTE', 'NPC_', 'NPOS', 'NPOT', 'NQUS', 'NQUT', 'NTOP', 'NTRM', 'NULL', 'NVER', 'NVMI', 'NVNM', 'NVPP', 'NVSI', 'OBND', 'OBTE', 'OBTF', 'OBTS', 'OCOR', 'OMOD', 'ONAM', 'OTFT', 'OVIS', 'PACK', 'PARW', 'PBAR', 'PBEA', 'PCMB', 'PCON', 'PDTO', 'PERK', 'PFIG', 'PFLA', 'PFO2', 'PFOR', 'PFPC', 'PFRN', 'PGRE', 'PHTN', 'PHWT', 'PHZD', 'PKC2', 'PKCU', 'PKDT', 'PKID', 'PKIN', 'PLCN', 'PLDT', 'PLVD', 'PLYR', 'PMIS', 'PNAM', 'POBA', 'POCA', 'POEA', 'PRCB', 'PRKC', 'PRKE', 'PRKF', 'PRKR', 'PRKZ', 'PROJ', 'PRPS', 'PSDT', 'PTDA', 'PTOP', 'PTRN', 'QNAM', 'QOBJ', 'QSDT', 'QSTA', 'QSTI', 'QTGL', 'QTOP', 'QUAL', 'QUST', 'RACE', 'RADR', 'RBPC', 'RCEC', 'RCLR', 'RCPR', 'RCSR', 'RCUN', 'RDAT', 'RDGS', 'RDMO', 'RDMP', 'RDOT', 'RDSA', 'RDWT', 'REFR', 'REGN', 'RELA', 'REPL', 'REPT', 'REVB', 'RFCT', 'RFGP', 'RGDL', 'RLDM', 'RNAM', 'RNMV', 'RPLD', 'RPLI', 'RPRF', 'RPRM', 'RVIS', 'SADD', 'SAKD', 'SAPT', 'SCCO', 'SCDA', 'SCEN', 'SCHR', 'SCOL', 'SCPT', 'SCQS', 'SCRL', 'SCRN', 'SCRO', 'SCSN', 'SCTX', 'SDSC', 'SGNM', 'SHOU', 'SHRT', 'SKIL', 'SLCP', 'SLGM', 'SMBN', 'SMEN', 'SMQN', 'SNCT', 'SNDD', 'SNDR', 'SNMV', 'SOFT', 'SOPM', 'SOUL', 'SOUN', 'SPCT', 'SPED', 'SPEL', 'SPGD', 'SPIT', 'SPLO', 'SPMV', 'SPOR', 'SRAC', 'SRAF', 'SSPN', 'STAG', 'STAT', 'STCP', 'STKD', 'STOL', 'STOP', 'STSC', 'SWMV', 'TACT', 'TCLT', 'TERM', 'TETI', 'TEND', 'TIAS', 'TIFC', 'TINC', 'TIND', 'TINI', 'TINL', 'TINP', 'TINT', 'TINV', 'TIQS', 'TIRS', 'TLOD', 'TNAM', 'TOFT', 'TPIC', 'TPLT', 'TPTA', 'TRDA', 'TRDT', 'TREE', 'TRNS', 'TSCE', 'TTEB', 'TTEC', 'TTED', 'TTEF', 'TTET', 'TTGE', 'TTGP', 'TVDT', 'TWAT', 'TX00', 'TX01', 'TX02', 'TX03', 'TX04', 'TX05', 'TX06', 'TX07', 'TXST', 'UNAM', 'UNES', 'UNWP', 'VATS', 'VCLR', 'VENC', 'VEND', 'VENV', 'VHGT', 'VISI', 'VMAD', 'VNAM', 'VNML', 'VTCK', 'VTEX', 'VTXT', 'VTYP', 'WAIT', 'WAMD', 'WATR', 'WBDT', 'WCTR', 'WEAP', 'WGDR', 'WKMV', 'WLEV', 'WLST', 'WMAP', 'WNAM', 'WOOP', 'WRLD', 'WTHR', 'WZMD', 'XACT', 'XALP', 'XAMC', 'XAPD', 'XAPR', 'XASP', 'XATP', 'XATR', 'XBSD', 'XCAS', 'XCCM', 'XCHG', 'XCIM', 'XCLC', 'XCLL', 'XCLP', 'XCLR', 'XCLW', 'XCMO', 'XCNT', 'XCRI', 'XCVL', 'XCVR', 'XCWT', 'XCZA', 'XCZC', 'XCZR', 'XDCR', 'XEMI', 'XESP', 'XEZN', 'XFVC', 'XGDR', 'XGLB', 'XHLP', 'XHLT', 'XHOR', 'XHTW', 'XIBS', 'XILL', 'XILW', 'XIS2', 'XLCM', 'XLCN', 'XLIB', 'XLIG', 'XLKR', 'XLKT', 'XLOC', 'XLOD', 'XLRL', 'XLRM', 'XLRT', 'XLTW', 'XLYR', 'XMBO', 'XMBP', 'XMBR', 'XMRC', 'XMRK', 'XMSP', 'XNAM', 'XNDP', 'XOCP', 'XORD', 'XOWN', 'XPDD', 'XPLK', 'XPOD', 'XPPA', 'XPRD', 'XPRI', 'XPRM', 'XPTL', 'XPWR', 'XRDO', 'XRDS', 'XRFG', 'XRGB', 'XRGD', 'XRMR', 'XRNK', 'XSCL', 'XSPC', 'XTEL', 'XTNM', 'XTRI', 'XWCN', 'XWCS', 'XWCU', 'XWEM', 'XWPG', 'XWPN', 'XXXX', 'YNAM', 'ZNAM', 'ZOOM'

# Filesystem helpers

function CreateFolder ([string]$Path) {
	if (!(Test-Path $Path))	{
        [void](New-Item -Path $Path -ItemType Directory)
    }
}

function CreateOrEmptyFolder ([string]$Path) {
	if (Test-Path $Path) {
        RoboRemove $Path
    } else {
        [void](New-Item -Path $Path -ItemType Directory)
    }
}

function GetFileCount ([string]$Path) {
    return (Get-ChildItem $Path -Recurse -File).Count
}

function GetSize ([string]$Path) {
    return (Get-ChildItem $Path -Recurse -File | Measure-Object -Property Length -Sum).Sum
}

function GetSizeInGigabytes ([string]$Path) {
    return (GetSize $ModsWorkspacePath) / 1024 / 1024 / 1024
}

function GetSizeOfSubsect ([string]$Path, [int]$Count) {
    return (Get-ChildItem $Path -Recurse -File | Select-Object -First $Count | Measure-Object -Property Length -Sum).Sum
}

function Rename ([string]$Path, [string]$NewName) {
	if (Test-Path $Path) {
        Rename-Item -Path $Path -NewName $NewName
    }
}

# 7zip Wrappers

function Extract7z ([string]$ArchivePath, [string]$Destination) {
	Write-Host ('Extracting ' + (Split-Path $ArchivePath -leaf) + '...')
	Invoke-Expression ('7z x -aoa -bso0 -o' + (Split-Path $Destination -leaf) + " `"$ArchivePath`"")
}

function Pack7z ([string]$ArchivePath, [string]$TargetPath, [switch]$EmptySource) {

	Write-Host ('Packing ' + (Split-Path $ArchivePath -leaf) + '...')
	Invoke-Expression ("7z a -mx9 -r -t7z -bso0 `"$ArchivePath`" `"$TargetPath\*`"")
	if ($EmptySource) {
        RoboRemove $TargetPath
    }
}

# BSArch Wrappers

function ExtractBA2 ([string]$ArchivePath, [string]$Destination, [switch]$RemoveArchive) {

    if (!(Get-Command 'OptimizeForSSD' -ErrorAction SilentlyContinue)) {
        . $using:PSScriptRoot/BaseConfiguration.ps1
	}

	Write-Host ('Extracting ' + (Split-Path $ArchivePath -leaf) + '...')

	if (OptimizeForSSD) {
		Invoke-Expression ("[void](.\dist\bsarch.exe unpack `"$ArchivePath`" `"$Destination`" -q)")
	} else {
		Invoke-Expression ("[void](.\dist\bsarch.exe unpack `"$ArchivePath`" `"$Destination`" -q -mt)")
	}
	if ($RemoveArchive) {
        Remove-Item $ArchivePath
    }
}

function PackBA2 ([string]$TargetPath, [string]$ArchivePath, [string]$Type, [switch]$IsSourceBA2, [switch]$RemoveSource) {

	if (Test-Path $TargetPath) {

	    if (!(Get-Command 'OptimizeForSSD' -ErrorAction SilentlyContinue)) {
            . $using:PSScriptRoot/BaseConfiguration.ps1
    	}

		Write-Host ('Packing ' + (Split-Path $ArchivePath -leaf) + '...')
		$TypeParams = ''

		switch ($Type) {
			'general'    { $TypeParams = '-fo4' }
			'compressed' { $TypeParams = '-fo4 -z' }
			'textures'   { $TypeParams = '-fo4dds -z' }
		}
		if (OptimizeForSSD) {
			Invoke-Expression ("[void](.\dist\BSArch.exe pack `"$TargetPath`" `"$ArchivePath`" $TypeParams)")
		} else {
			Invoke-Expression ("[void](.\dist\BSArch.exe pack `"$TargetPath`" `"$ArchivePath`" $TypeParams -mt)")
		}
		if ($IsSourceBA2)  {
            (Get-Item $ArchivePath).LastWriteTime = '01/11/2008 00:00:00'
        }
		if ($RemoveSource) {
		    RoboRemove $TargetPath
            Remove-Item $TargetPath -Recurse
        }
	}
}

# JSON File Helpers

function ExportToJSONFile ($Object, [string]$Path) {
	$Object | ConvertTo-JSON | Out-File -FilePath $Path -Force
}

function ImportFromJSONFile ([string]$Path) {
	Return Get-Content -Raw -Path $Path | ConvertFrom-JSON
}

# Robocopy wrappers

function RoboCopyCustom ([string]$From, [string]$To) {
	Invoke-Expression ("[void](robocopy '$From' '$To' /is /it /im /mt:" + (GetParallelRobocopyLimit) + " /s /z /nfl /ndl /njh /njs /nc /ns /np)")
}

function RoboMove ([string]$From, [string]$To, [switch]$RetainSource) {
	Invoke-Expression ("[void](robocopy '$From' '$To' /is /it /im /move /mt:" + (GetParallelRobocopyLimit) + " /s /z /nfl /ndl /njh /njs /nc /ns /np)")

	if (Test-Path $From) {
		RoboRemove $From
		if (!$RetainSource) {
			Remove-Item $From
		}
	} elseif ($RetainSource) {
		[void](New-Item -Path $From -ItemType Directory)
	}
}

function RoboOverride ([string]$From, [string]$To) {
	Invoke-Expression ("[void](robocopy '$From' '$To' /is /it /im /move /mt:" + (GetParallelRobocopyLimit) + " /s /xl /z /nfl /ndl /njh /njs /nc /ns /np)")
}

function RoboRemove ([string]$Path) {
	Invoke-Expression ("[void](robocopy '$TrashPath' '$Path' /mir /w:0 /r:0 /mt:" + (GetParallelRobocopyLimit) + " /nfl /ndl /njh /njs /nc /ns /np)")
}

# Progress/Task helpers

function IntimateAllTasksCompletion ([string]$Message, [switch]$NoExitWrapper) {

	Write-Host "`n$Message" -ForegroundColor Green
	Write-Host ('Completed in: ' + ($(Get-Date) - $StartTime).Minutes + ' minutes')

	if (!$NoExitWrapper) {
		Write-Host "`n`nPress enter to exit..." -ForegroundColor Green -NoNewLine
		Read-Host
	}
}

function IntimateError ([string]$Message, [string]$Help = '', [switch]$CanContinue) {

	[System.Media.SystemSounds]::Hand.Play()
	if ($CanContinue) {
	    LineBreak
	    Write-Host $Message -ForegroundColor Red
    	Write-Host $Help -ForegroundColor Red
    	LineBreak
	    Write-Host "Press enter to continue..." -ForegroundColor Yellow -NoNewLine
	    Read-Host
	} else {
	    throw $Message
	}
}

function IntimateLongWaitTime {
	Write-Host "`nThe script may now take a long time to workout its tasks..."
}

function IntimateSubTaskStart ([string]$Message) {
	Write-Host $Message -ForegroundColor Cyan
}

function IntimateSubTaskEnd {
	Write-Host 'Done.' -ForegroundColor Cyan
}

function IntimateTaskError ($ErrorRecord) {

	[System.Media.SystemSounds]::Hand.Play()
	Write-Host "`nAn error has occurred! Screenshot the following enclosed white text to file as bug:" -ForegroundColor Yellow
	Write-Host '--Error--' -ForegroundColor Yellow
	Write-Host ($_ | Out-String)
	Write-Host '--Error--' -ForegroundColor Yellow
	Write-Host "Press enter to exit..." -ForegroundColor Yellow -NoNewLine
	Read-Host
}

function IntimateTaskStart ([string]$Message) {
	Write-Host "`n$Global:CurrentTask/$Global:TotalTasks - $Message" -ForegroundColor Green
	$Global:CurrentTask++
}

function IntroduceScript {
	cls
	Write-Host "`n----------------------------------" -ForegroundColor Yellow
	Write-Host "Merge Scripts By Brazenvoid" -ForegroundColor Yellow
	Write-Host "----------------------------------`n" -ForegroundColor Yellow
	Write-Host "$Global:ScriptName Script" -ForegroundColor Yellow
}

function LineBreak {
	Write-Host ''
}

# Read-Host helpers

function Confirm ([string]$Subject, [string]$Help, [switch]$DefaultYes) {
    if ($DefaultYes) {
        Return (Inquire -Subject $Subject -Help $Help -Question "y/n" -DefaultValue 'y') -ne 'n'
    }
	Return (Inquire -Subject $Subject -Help $Help -Question "y/n" -DefaultValue 'n') -eq 'y'
}

function Inquire ([string]$Subject, [string]$Help, [string]$Question, [string]$DefaultValue = '') {

	if ($DefaultValue -ne '') {
        $Question = "$Question [$DefaultValue]"
    }

	Write-Host "`n$Subject" -ForegroundColor Cyan
	Write-Host $Help
	Write-Host ("$Question" + ': ') -ForegroundColor Cyan -NoNewLine
	$value = Read-Host

	if (($value -eq '') -and ($DefaultValue -ne '')) {
        Return $DefaultValue
    }
	Return $value
}

function InquireLoadOrderManagement ([string]$Path) {

	if (Confirm -Subject 'Would you like to manage your load order now?' -Help 'Opens the mods folder.') {
		Invoke-Item $Path
		Write-Host 'Press enter when done...' -ForegroundColor Cyan -NoNewLine
		Read-Host
	}
}

function InquirePath ([string]$Subject, [string]$Help, [string]$DefaultValue = '') {

	$Path = Inquire -Subject $Subject -Help $Help -Question 'Path' -DefaultValue $DefaultValue
	if ($Path[0] -eq '"') {
		$Path = $Path.Remove(0, 1)
		$Path = $Path.Remove($Path.Length - 1, 1)
	}
	$Path = [Management.Automation.WildcardPattern]::Escape($Path)

	if (!(Test-Path $Path)) {
		InquirePath -Subject 'Please specify the correct path.' -Help $Help -DefaultValue $DefaultValue
	}
	Return $Path
}

function IntimateAction ([string]$Action, [string]$Help) {
	Write-Host "`n$Action" -ForegroundColor Magenta
	Write-Host $Help
	Write-Host 'Press enter when done...' -ForegroundColor Cyan -NoNewLine
	Read-Host
}

function IntimateFilesystemAction ([string]$Action, [string]$Help = '', [string]$Path) {

	if  ($Help -ne '') {
		Write-Host "`n$Action" -ForegroundColor Magenta
		Write-Host $Help -NoNewLine
	} else {
		Write-Host "`n$Action" -ForegroundColor Magenta -NoNewLine
	}
	Read-Host
	Invoke-Item $Path
	Write-Host 'Press enter when done...' -ForegroundColor Cyan -NoNewLine
	Read-Host
}

function MultiChoice ([string]$Subject, [array]$Options, [string]$OptOutLabel = '', [int]$Default = -1, [switch]$Cancellable) {

	$HasOptOut = $OptOutLabel -ne ''
	$HasDefault = $Default -ne -1

	if (!$HasOptOut -and ($Options.Length -eq 0) -and !$Cancellable) {
        throw 'MultiChoice structure requires at least one defined option.'
    }

	Write-Host "`n$Subject`n" -ForegroundColor Green

	for ($i = 1; $i -le $Options.Count; $i++) {
		Write-Host ("[$i] " + $Options[$i - 1]) -ForegroundColor Green
	}

	if ($HasOptOut) {
		Write-Host "[0] $OptOutLabel" -ForegroundColor Green
	}

	if ($HasDefault) {
		Write-Host ("`nChoose [" + $Options[$Default - 1] + "]: ") -ForegroundColor Green -NoNewLine
		$OpChoice = Read-Host
		$Chosen = $OpChoice -eq '' ? $Default.ToString() : $OpChoice
	} elseif ($Cancellable) {
	    Write-Host ("`nChoose [Cancel]: ") -ForegroundColor Green -NoNewLine
        $OpChoice = Read-Host
        $Chosen = $OpChoice -eq '' ? $False : $OpChoice
	} else {
		Write-Host "`nChoose: " -ForegroundColor Green -NoNewLine
		$Chosen = Read-Host
	}

	Return $Chosen -eq $False ? $False : [Convert]::ToInt32($Chosen, 10)
}