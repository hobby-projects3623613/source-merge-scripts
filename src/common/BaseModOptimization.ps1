# Package: 		Merge Scripts
# Author:  		Brazenvoid
# Description: 	Performs mod structural optimizations

$ModsOptimizeDataPath   = "$ModsOptimizePath\Data"
$OptimizedModIdentifier = 'optimized600'
$PreviousModIdentifier  = 'optimized310'

function HandleArbitraryFolderInsideModArchive {

	$HadComplexStructure = $false

	if (((Get-ChildItem $ModsOptimizePath -Include *.es?, *.ba2).Count -eq 0) -and ((Get-ChildItem $ModsOptimizePath -Directory).Count -eq 1)) {

		$Folder = (Get-ChildItem $ModsOptimizePath -Directory)[0]

		if (!($F4AssetFolders -Contains $Folder.Basename)) {

			$FolderFullName = $Folder.FullName
			$FolderDataPath = $FolderFullName + '\Data'
			$HadComplexStructure = $true

			if (Test-Path $FolderDataPath) {
				Get-ChildItem ($FolderDataPath) | Move-Item -Destination $ModsOptimizePath
			} else {
				Get-ChildItem ($FolderFullName) | Move-Item -Destination $ModsOptimizePath
			}

			Remove-Item $FolderFullName -Recurse
		}
	}
	return $HadComplexStructure
}

function HandleBA2sInsideModArchive {

	$HadBA2s = (Get-ChildItem $ModsOptimizePath -Filter *.ba2).Count -gt 0
    if ($HadBA2s) {

        Get-ChildItem $ModsOptimizePath -Filter *.ba2 | ForEach-Object -ThrottleLimit (GetParallelBSArchLimit) -Parallel {
            . $using:PSScriptRoot/BaseScript.ps1
            ExtractBA2 -ArchivePath $_.FullName -Destination $ModsOptimizePath -RemoveArchive
        }
	}
	return $HadBA2s
}

function HandleDataFolderInsideModArchive () {

	$HadDataFolderInside = Test-Path $ModsOptimizeDataPath
	if ($HadDataFolderInside) {
		Get-ChildItem $ModsOptimizeDataPath | Move-Item -Destination $ModsOptimizePath
		Remove-Item $ModsOptimizeDataPath
	}
	return $HadDataFolderInside
}

function HandleLoaderPluginsInsideModArchive {

	$HadLoaderPlugin = $false

	foreach ($Plugin in (Get-ChildItem $ModsOptimizePath -Filter *.es?)) {

		if ($Plugin.Length -lt 500) {
			$Found = $false

			forEach ($Key in $F4PluginKeys) {
				$Found = (Select-String -Path $Plugin.FullName -Pattern $Key).Length
				if ($Found) {
					break
				}
			}
			if (!$Found) {
				$HadLoaderPlugin = $true
				Remove-Item $Plugin.FullName
			}
		}
	}
	return $HadLoaderPlugin
}

function HandleLooseAssetsInModArchive {

    $Found = $false;
    foreach ($AssetFolder in $F4AssetFolders) {

        if (Test-Path "$ModsOptimizePath\$AssetFolder") {
            $Found = $true
            break
        }
    }
    return $Found
}

function OptimizeModArchive ([string]$ModsSourcePath, $Archive, [bool]$ArchiveOptimizedMods) {

	IntimateSubTaskStart 'Optimizing new mod...'

	# Analysis

    $IsFolder = $Archive.PSIsContainer

	$CleanedArchiveName = $Archive.Basename.Replace('"', '').Replace("'", '')
	if ($Archive.Basename -ne $CleanedArchiveName) {
	    if ($IsFolder) {
		    Rename-Item -Path $Archive.FullName -NewName $CleanedArchiveName
		} else {
		    Rename-Item -Path $Archive.FullName -NewName ($CleanedArchiveName + $Archive.Extension)
		}
	}
    $ArchivePath = $Archive.FullName.Replace($Archive.Basename, $CleanedArchiveName)

	if ($IsFolder) {
		$IsNot7z = $true

		if ($ArchiveOptimizedMods) {
		    RoboMove -From $ArchivePath -To $ModsOptimizePath
		} else {
		    RoboCopyCustom -From $ArchivePath -To $ModsOptimizePath
		}
	} else {
		$IsNot7z = $Archive.Extension -ne '.7z'
		Extract7z -ArchivePath $ArchivePath -Destination $ModsOptimizePath
	}

	# Processing

    $HasLooseAssetsAtFirstLevel = HandleLooseAssetsInModArchive
	$HadBA2s = HandleBA2sInsideModArchive
    $HadLoaderPlugin = HandleLoaderPluginsInsideModArchive

    if (!($HasLooseAssetsAtFirstLevel -or $HadBA2s -or $HadLoaderPlugin)) {

        $HadDataFolderInside = HandleDataFolderInsideModArchive

        if (!$HadDataFolderInside) {
            $HadComplexStructure = HandleArbitraryFolderInsideModArchive

            if (!$HadComplexStructure) {
                IntimateError `
                -Message "Script was not able to determine this mod's structure." `
                -Help "You can either fix it yourself or terminate the script and remove the mod from the load order. `nAny existing template has not been touched yet. `nPress enter once you have optimized the mod..." `
                -CanContinue
            }
        }

        $HadBA2s = HandleBA2sInsideModArchive
        $HadLoaderPlugin = HandleLoaderPluginsInsideModArchive
    }

    $NewArchiveName = $CleanedArchiveName.ToLower().Replace('-' + $PreviousModIdentifier, '') + '-' + $OptimizedModIdentifier + '.7z'

	# Generating Archives

	if ($HadBA2s -or $HadDataFolderInside -or $HadLoaderPlugin -or $IsNot7z) {

	    if ($ArchiveOptimizedMods) {

		    Pack7z -ArchivePath "$ModsSourcePath\$NewArchiveName" -TargetPath $ModsOptimizePath

		    if (!$Archive.PSIsContainer) {
                Remove-Item $ArchivePath -Recurse
            }
		}
	} else {
		Write-Host 'No optimizations required.'
		Rename-Item -Path $ArchivePath -NewName $NewArchiveName
	}

	Write-Host 'Overwriting mods template...'
	RoboMove -From $ModsOptimizePath -To $ModsWorkspacePath -RetainSource

	IntimateSubTaskEnd
}
 #>