# Package: Merge Scripts
# Author:  Brazenvoid

# Management

function LoadConfiguration {
    $Global:Configuration = ImportFromJSONFile $ConfigurationPath
}

function OverwriteConfiguration ($Configuration) {
    ExportToJSONFile $Configuration $ConfigurationPath
    $Global:Configuration = $Configuration
}

function SaveConfiguration {
    ExportToJSONFile $Global:Configuration $ConfigurationPath
}

# Wrappers

function GetMaximumBA2Size {
    Return $Global:Configuration.fallout4.maximumBA2Size
}

function GetParallelBSArchLimit {
    Return $Global:Configuration.performance.parallelBSArch
}

function GetParallelRobocopyLimit {
    Return $Global:Configuration.performance.parallelRobocopy
}

function OptimizeForSSD {
    Return $Global:Configuration.performance.isSSD
}

# Performance Profiling

function GetCoreCount () {
    return [Int][Math]::Floor((Get-CimInstance -ClassName 'Win32_Processor' | Measure-Object -Property 'NumberOfCores' -Sum).Sum)
}

function ValidateSSDVolume ([string]$Path) {

    $DriveLetter = $Path[0]
    $IsSSD       = $False

    Write-Host 'There might be some harmless errors as the script tries to find the type of your disk drive...'
    LineBreak

    foreach ($Drive in Get-PhysicalDisk) {
        if ((($Drive | Get-Disk | Get-Partition).DriveLetter -Contains $DriveLetter) -and ($Drive.MediaType -eq 'SSD')) {
            $IsSSD = $True
            break
        }
    }
    return $IsSSD
}

# Initialization

LoadConfiguration
 #>