# Package:      Merge Scripts
# Author:       Brazenvoid
# Description: 	Common merge methods

# Environment

$MergedSourceModName              = 'SourceMergeExtras'
$MergedModESLPluginPath           = '.\dist\' + $MergedSourceModName + '.esl'
$MergedModESLFlaggedESPPluginPath = '.\dist\'+ $MergedSourceModName + 'Flagged.esp'
$MergedModESPPluginPath           = '.\dist\' + $MergedSourceModName + '.esp'

$MergedModArchives = @(
	@{Filename = $MergedSourceModName + ' - Textures.ba2';       Folder = @('Textures');         Type = 'archive';           Compression = 'textures'}
	@{Filename = $MergedSourceModName + ' - Texturesxx.ba2';     Folder = @('Textures');         Type = 'archive';           Compression = 'textures'}
	@{Filename = $MergedSourceModName + ' - Main.ba2';           Folder = @('Main');             Type = 'archive';           Compression = 'general'}
	@{Filename = $MergedSourceModName + '.esl';                  Folder = '';                    Type = 'plugin';            Compression = 'n/a'}
	@{Filename = $MergedSourceModName + ' - Plugins.7z';         Folder = '';                    Type = 'plugins-archive';   Compression = 'n/a'}
)

enum MergedModArchivesEnum {
	Textures
	TexturesMulti
	Main
	Plugin
	PluginsArchive
}

# Logic

function FindFilesWithAppropriateSizeForArchival([int]$FilesCount, [int]$AffirmedCount = 0) {

	if (($FilesCount - $AffirmedCount) -gt 100) {

		do {
			$FilesCount = [Math]::Floor(($FilesCount - $AffirmedCount) * 0.5) + $AffirmedCount
			$Size = GetSizeOfSubsect -Path $SourcePath -Count $FilesCount
		} while ($Size -gt $MaxAssetsSize)

		if (($FilesCount - $AffirmedCount) -gt 100) {
			$FilesCount =
			FindFilesWithAppropriateSizeForArchival -FilesCount (($FilesCount * 2) - 1) -AffirmedCount $FilesCount
		}
	}
	return $FilesCount
}

function GenerateMergeModArchives ([string]$ModName) {

	# Analyzing assets distribution

	$MaximumTexturesSize     = GetMaximumBA2Size
	$TexturesPath 		 	 = "$ModsWorkspacePath\" + $MergedModArchives[[MergedModArchivesEnum]::Textures].Folder[0]
	$TexturesExist           = Test-Path $TexturesPath
	$UseMultiTextureArchives = $TexturesExist -and ((GetSize $TexturesPath) -gt $MaximumTexturesSize)

	if ($TexturesExist -and !$UseMultiTextureArchives) {

		if ($UseMultiTextureArchives) {
			PrepareTextureAssetsForArchival -ModName $ModName
		} else {
			PrepareAssetsForArchival `
			-ModName $ModName `
			-Archive $MergedModArchives[[MergedModArchivesEnum]::Textures] `
			-AssetFolders $MergedModArchives[[MergedModArchivesEnum]::Textures].Folder
		}
	}

	PrepareAssetsForArchival `
    -ModName $ModName `
    -Archive $MergedModArchives[[MergedModArchivesEnum]::Main] `
    -AssetFolders (Get-ChildItem $ModsWorkspacePath -Directory | Select-Object -ExpandProperty BaseName)

	# Generation

	Get-ChildItem $ModsOptimizePath -Directory | ForEach-Object -ThrottleLimit (GetParallelBSArchLimit) -Parallel {

		. $using:PSScriptRoot/BaseScript.ps1
		. $using:PSScriptRoot/BaseMerge.ps1

		$NewArchivePath = $pwd.Path + $ModsWorkspacePath.Substring(1) + '\' + $_.BaseName + '.ba2'

		if ($using:UseMultiTextureArchives -and $_.BaseName.Contains('Textures')) {
			$Archive = $MergedModArchives[[MergedModArchivesEnum]::TexturesMulti]
		} else {
			$Archive = $MergedModArchives[[MergedModArchivesEnum]$_.BaseName.Replace($using:ModName + " - ", '')]
		}

		PackBA2 -TargetPath $_.FullName -ArchivePath $NewArchivePath -Type $Archive.Compression -RemoveSource
	}
}

function PrepareAssetsForArchival([string]$ModName, $Archive, [string[]]$AssetFolders) {

	$DestinationPath =
	("$ModsOptimizePath\" + $Archive.Filename.Substring(0, $Archive.Filename.Length - 4)).
			Replace($MergedSourceModName, $ModName)

	foreach ($AssetFolder in $AssetFolders) {

		$SourcePath = "$ModsWorkspacePath\$AssetFolder"

		if (Test-Path $SourcePath) {
			CreateFolder $DestinationPath
			Move-Item -Path $SourcePath -Destination $DestinationPath -Force
		}
	}
}

function PrepareTextureAssetsForArchival([string]$ModName) {

	$Archive       = $MergedModArchives[[MergedModArchivesEnum]::TexturesMulti]
	$ArchiveIndex  = 1
	$SourcePath    = "$ModsWorkspacePath\" + $Archive.Folder[0]
	$MaxAssetsSize = GetMaximumBA2Size

	do {
		$DestinationFolder =
		$Archive.Filename.Substring(0, $Archive.Filename.Length - 6) +
				$ArchiveIndex.ToString('00')

		Write-Host ('Preparing assets for ' + $DestinationFolder + '.ba2...')

		$DestinationPath = ("$ModsOptimizePath\$DestinationFolder").Replace($MergedSourceModName, $ModName)
		CreateFolder $DestinationPath

		$FilesCount = GetFileCount $SourcePat
		$Size       = GetSize $SourcePath

		if ($Size -le $MaxAssetsSize) {
			Move-Item -Path $SourcePath -Destination $DestinationPath -Force
			break
		}

		$FilesCount = FindFilesWithAppropriateSizeForArchival $FilesCount

		Get-ChildItem $SourcePath -Recurse -File |
				Select-Object -First $FilesCount |
				ForEach-Object -ThrottleLimit (GetParallelRobocopyLimit) -Parallel {

					$SourcePath      = ($using:ModsWorkspacePath).Substring(2)
					$DestinationPath = ($using:DestinationPath).Substring(2)
					$NewPath         = $_.Fullname.Replace($SourcePath, $DestinationPath, $True, $Null)

					[void](New-Item -Path (Split-Path -Path $NewPath) -ItemType Directory -Force)
					Move-Item -Path $_.Fullname -Destination $NewPath -Force
				}

		$ArchiveIndex++
	} while (GetFileCount $SourcePath)
}