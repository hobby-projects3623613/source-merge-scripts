# Package: 		Merge Scripts
# Author:  		Brazenvoid
# Description: 	Creates sources archive and sets up work folders

. $PSScriptRoot/BaseScript.ps1
. $PSScriptRoot/BaseConfiguration.ps1

$Global:ScriptName = 'Setup'
$Global:TotalTasks = 3

IntroduceScript
try {
	# -----------------
	IntimateTaskStart 'Setting up work folders...'

	CreateOrEmptyFolder $ModsOptimizePath
	CreateFolder 		$ModsToMergePath
	CreateOrEmptyFolder $ModsWorkspacePath
	CreateFolder		'.\output'
	CreateFolder		$TrashPath

	# -----------------
    IntimateTaskStart 'Gathering information...'

    $MaximumBA2Size =
        Inquire `
        -Subject 'Maximum size of texture BA2s' `
        -Help "If you don't know about it then the default 4 GB is a safe bet." `
        -Question 'Please enter limit in GBs' `
        -DefaultValue 4.0

	# -----------------
	IntimateTaskStart 'Setting up performance profile...'

	$DriveLetter = $pwd.Path[0]
	$IsSSD = $False
	$ParallelBSArch = 2
	$ParallelRobocopy = 8

	if (ValidateSSDVolume $pwd.Path) {
	    $IsSSD     = $True
	    $CoreCount = GetCoreCount

        if ($CoreCount -le 16) {
            $ParallelBSArch = $CoreCount - 1
        } else {
            $ParallelBSArch = 32
        }
        $ParallelRobocopy = 128
	}

	$Configuration = @{
	    fallout4 = @{
            maximumBA2Size = [Math]::Floor([Convert]::ToSingle($MaximumBA2Size) * 1.4 * 1073741824)
        }
		performance = @{
			isSSD = $IsSSD
			parallelBSArch = $ParallelBSArch
			parallelRobocopy = $ParallelRobocopy
		}
	}
	OverwriteConfiguration $Configuration

	IntimateAllTasksCompletion 'Setup completed successfully!'
}
catch {	IntimateTaskError $_ } 