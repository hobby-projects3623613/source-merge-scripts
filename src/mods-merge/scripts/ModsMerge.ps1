# Package: 		Merge Scripts
# Author:  		Brazenvoid
# Description: 	Performs the sources merger

. $PSScriptRoot/BaseScript.ps1
. $PSScriptRoot/BaseConfiguration.ps1
. $PSScriptRoot/BaseModOptimization.ps1
. $PSScriptRoot/BaseMerge.ps1

$Global:ScriptName = 'Mods Merge'
$Global:TotalTasks = 3
$OutputPath = '.\output'

IntroduceScript
try {
    Function BuildModdedTemplate ([string]$ModsSourcePath, [bool]$ArchiveOptimizedMods = $true) {

        $ModCounter     = 1
        $TotalModsCount = (Get-ChildItem $ModsSourcePath).Count
        $NewModsCount   = (Get-ChildItem $ModsSourcePath -Exclude "*-$OptimizedModIdentifier.*").Count

        Write-Host ('Found: ' + $TotalModsCount.ToString() + ' mods, ' + $NewModsCount.ToString() + " mods may need optimization.`n")

        Get-ChildItem $ModsSourcePath | Sort-Object | ForEach-Object {

            Write-Host ($ModCounter.ToString() + '/' + $TotalModsCount.ToString() + ' - Validating ' +  $_.Basename + '...') -ForegroundColor Magenta

            if ($_.Basename -NotLike "*-$OptimizedModIdentifier") {
                OptimizeModArchive -ModsSourcePath $ModsSourcePath -Archive $_ -ArchiveOptimizedMods $ArchiveOptimizedMods
            } else {
                Extract7z -ArchivePath $_.FullName -Destination $ModsWorkspacePath
            }

            $ModCounter++
        }
    }

	# -----------------
	IntimateTaskStart 'Gathering Information...'

    RoboRemove $ModsWorkspacePath
    RoboRemove $ModsOptimizePath

    InquireLoadOrderManagement -Path $ModsToMergePath

	$ModName =
	    Inquire `
        -Subject 'Would you like to name your merged mod?'`
        -Help "The name is for the BA2s and the loader plugin."`
        -Question 'Name'`
        -DefaultValue 'MergedMod'

	$OptimizeMods =
	    Confirm `
	    -Subject 'Do you intend to reuse the mods for further or later merges?'`
	    -Help 'Archives the optimized mods for faster merges later.'`
	    -DefaultYes

	$ArchiveAssets =
	    Confirm `
	    -Subject 'Do you want to create BA2s for the merged mod?'`
	    -Help 'Either have BA2s or loose files.'`
	    -DefaultYes

    if ($ArchiveAssets) {
        $PluginType =
            MultiChoice `
            -Subject 'Choose loader plugin type for extra file archives:'`
            -Options @('ESP', 'ESL', 'ESL Flagged ESP', "Don't add a loader")`
            -Default 3
    }

    $ArchiveMod =
        Confirm `
        -Subject 'Pack mod into an archive?'`
        -Help 'Compresses mod files into a 7z archive'`
        -DefaultYes

    $Inspection =
        Confirm `
        -Subject 'Do you want to inspect the final mod template before packing?'`
        -Help 'This step can help you identify undetected loader plugins or final manual overrides or corrections.'

	# -----------------
	IntimateTaskStart 'Building modded template...'

    BuildModdedTemplate -ModsSourcePath $ModsToMergePath -ArchiveOptimizedMods $OptimizeMods

    if ($Inspection) {
        IntimateFilesystemAction `
        -Action 'Now you can inspect/modify the merged mod template.'`
        -Help 'Press enter to open the workspace folder...'`
        -Path $ModsWorkspacePath
    }

	# -----------------
	IntimateTaskStart 'Generating merged mod...'

	if ($ArchiveAssets) {
	    GenerateMergeModArchives $ModName
	}

    if ($PluginType -lt 4) {
        switch ($PluginType) {
            1 { Copy-Item -Path $MergedModESPPluginPath -Destination "$ModsWorkspacePath\$ModName.esp" -Force }
            2 { Copy-Item -Path $MergedModESLPluginPath -Destination "$ModsWorkspacePath\$ModName.esl" -Force }
            3 { Copy-Item -Path $MergedModESLFlaggedESPPluginPath -Destination "$ModsWorkspacePath\$ModName.esp" -Force }
        }
	}

    if ($ArchiveMod) {
        Pack7z -ArchivePath "$OutputPath\$ModName.7z" -TargetPath $ModsWorkspacePath
    } else {
        RoboMove -From $ModsWorkspacePath -To $OutputPath
    }

	RoboRemove $ModsWorkspacePath
	Invoke-Item $OutputPath

	# -----------------
	IntimateAllTasksCompletion 'Merge completed successfully!'
}
catch {	IntimateTaskError $_ }