Mods with installers have a very different file structure and I cannot know what you would like to install. So you need to install it through your mod manager and use the resulting installation.

# Instructions

This guide assumes that you use ***Vortex*** as mod manager.

1. Install the mod through the usual means.
2. Right-click the mod in the ***Mods*** page and click on the ***Open in File Manage*** option.
3. ***Go up*** and ***copy the folder*** the files are contained inside.
4. Paste this folder in the ***mods*** folder for source merge or ***mods-merge*** folder for merge mods.
5. Rename it according to your ***naming scheme***.
6. ***Remove the mod*** from Vortex.