# Overview

Usually this occurs when you do not have 7zip installed but for some users, it may occur regardless.

It occurs when the 7zip application is not accessible or registered in windows for use by other programs.

# How to fix

First and foremost, try ***reinstalling 7zip***, and check whether it is working by using the method described in the ***Validate the fix*** section. Follow the following steps when this fails.

1. Search ***Environment Variables*** in search bar.
2. Click on ***Edit the system environment variables***.
3. Click the bottom button named ***Environment Variables***.
4. In the ***first (your user) / second box (all users)***, double-click the Path entry.
5. If you already know the path to the 7zip installation folder then click ***New ***and ***paste or type*** the path in the newly created row. Remember this interface only takes folders so do not add ***7z.exe*** at the end.
6. Otherwise,  click ***Browse*** and pick the folder by hand. (Default: C:\Program Files\7-Zip)
7. Exit all dialogues by clicking ***OK***.

![](https://staticdelivery.nexusmods.com/mods/1151/images/53420/53420-1631383920-940027152.png)

# Validate the fix

1. Search ***PowerShell*** in the search bar.
2. Click on ***Windows PowerShell***.
3. Type ***7z*** and press enter.
4. If a lot of text is shown in white then all is good.
5. If only ***red lines*** are shown then double check with the above screenshot.
6. If all seems in order then do a ***computer restart*** and do this again.
7. If it still doesn't work then you can try reaching out to me directly on my ***discord***.