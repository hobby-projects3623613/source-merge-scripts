This guide will give all parameters you need to set for the ***Source Merge Script*** to be added as a tool in Vortex for easy access. Other scripts have similar mechanism.

1. Go to Dashboard in Vortex and click ***Add Tool*** button.
2. In the subsequent dialogue set:
   1. ***Name*** as ***Source Merge*** or whatever you like.
   2. ***Target*** as ***C:\Program Files\PowerShell\7\pwsh.exe***
   3. ***Command Line*** as ***-NoProfile -ExecutionPolicy Bypass -Command "& .\dist\SourceMerge.ps1"***
   4. ***Start In*** as path to the ***Source Merge Scripts*** folder.
   5. Turn on the switch ***Run in shell***
   6. Click ***Save***