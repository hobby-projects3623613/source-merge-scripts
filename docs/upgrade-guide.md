***Setup*** script must always be run after overwriting package files.

# v7.0.0 to v7.3.1

Verify the optimized mod archives to have the usual asset folders. Otherwise, You will have to re-download and merge them. This problem manifested in mods with BA2 and loose files in the first level.

# v6.3.0 to v7.0.0

As the configuration storage mechanism was changed in this version, performance assessment has to be redone. To do this, run the ***Setup*** script once again.

It will ask for your current deployed template, if you don't remember, you can look it up by opening the ***Configuration.ini*** file in ***dist*** folder in notepad.

Setup script now is configured to fix package structure and reassess performance constants when at least one template exists.

# v6.2.0 to v6.3.0

Create a folder named ***trash*** inside the ***dist*** folder.

# v5.0.2 to v6.0.0

v6 is incompatible with previous versions due to the arbitrary results produced by a bug in a certain common scenario. Sorry, but to be on safe side, existing users must rebuild their load order with fresh copies of the mods. 

A patch work is possible for advanced users, with:
1. Replacing only those mods that were sourced with ***loose files***, and they had only one folder like "Textures" or "Meshes" at the ***first level*** in their archive i.e. not inside a Data or any arbitrary folder.
2. Opening each optimized archive and deleting any ***loader ESP or ESL*** inside. They are typically less than ***150 bytes***.