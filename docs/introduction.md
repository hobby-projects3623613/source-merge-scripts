# What is source merge?

## The problem

All mainstream Bethesda games have had extensive modding capabilities, the way they achieve this has changed little over the years. With it the engines have also been very slow in getting optimized.

The game essentially browses all of its own files (~500k) and the mods. It constructs a set of tables to resolve things according to the load order. This happens every time you start the game, load a save or simply go to a new area. The vast your mods are, the slower it gets and the more stutter you experience. Also contrary to common sense, once this table is constructed, the engine still is much slower to get something from a mod.

## The idea

This package essentially circumvents the problem by merging your mods into the game's own archives i.e. your modded state becomes the new vanilla. It is just like playing the game with your mods but with the vanilla's smoothness.

As with many texture overhauls, they touch upon thousands of files, some even tens of thousands. All that workload capacity is effectively vacated for anything more you want to add to the game. Similarly many mods have plugins that are there only to tell the game to load the related asset archives, those are removed and you get more mod slots for reuse.

**Source:**  [GitLab Repository](https://gitlab.com/hobby-projects3623613/source-merge-scripts)

# Features

## Templates 

Make templates out of your favourite texture combinations with near instantaneous switching (when package is in the same volume as the game).

You can also use these to experiment and also to have different flavours of experience in the same playthrough.

## Sources Cache

Creates a cache for faster subsequent merges. 

This also serves as the backup for any mistakes you make and thus makes resets easy even in the worst cases of corrupt package.

## Full Mod Support

Merge mods that introduce new functions as well as replacements. 

Assets not in the source are merged into BA2s with a loader ESL which gets loaded into the game data folder as part of the merge. You'll need to enable the ESL in your mod manager for them to work.

Currently, no tools exist to merge plugins reliably through scripts so one has to do this manually with [FO4Edit](https://www.nexusmods.com/fallout4/mods/2737/) or [ZEdit](https://github.com/z-edit/zedit) or not do it.

Extra plugins are thus packed into a ***SourceMergeExtras.7z*** in the data folder, and you will have to load it in your mod manager. This archive may also contain any incompatible files or folders referenced by the plugins. ***Do not delete this file from the data folder.***

## Mod Archive Structures

Numerous mod structures are handled by the script automatically. All mods fed to it are optimized on first use to a simple structure for faster merges. 